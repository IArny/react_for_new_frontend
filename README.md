Создано 2 компонента:

- ProductList - блок с карточками товара, загружает json, выводит карточки

- ProductListItem - блок с информацией о товаре

1 action:

- UPDATE_PRODUCT_LIST - вызывается при необходимости обновления списка

1 reducer:

- ProductList - обновляет состояние списка (создает новый из полученного json)

Запуск:
```
npm install
npm run watch
```
или
```
npm install http-server -g
http-server app
```

Время:

30 минут - изучение json api сервиса

2 часа - реализация компонентов
