const webpack = require('webpack');
const path = require('path');

const BUILD_DIR = path.resolve(__dirname, 'app');
const SRC_DIR = path.resolve(__dirname, 'src');
const NODE_ENV = process.env.NODE_ENV || 'production';

const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractSass = new ExtractTextPlugin({
    filename: "[name].[contenthash].css",
    disable: process.env.NODE_ENV === "development"
});

const config = {
	context: SRC_DIR,
	entry: {
		init: './init',
		main: './main'
	},
	output: {
		path: BUILD_DIR,
		filename: './[name].js',
		library: 'main'
	},

	resolve: {
		extensions: ['.js', '.jsx', '.less']
	},

	watch: NODE_ENV == 'development',

	devtool: NODE_ENV == 'development' ? 'cheap-inline-module-source-map' : '',
	devServer: {
		contentBase: './app',
		historyApiFallback: true
	},
	module: {
		loaders: [
			{
				test: /\.jsx?$/,
				// include: /src\//,
				loader: ['babel-loader'],
				exclude: /node_modules/,
			}, {
	        test: /(\.css)$/,
	        use: [{
	            loader: "style-loader" // creates style nodes from JS strings
	        }, {
	            loader: "css-loader" // translates CSS into CommonJS
	        }],
      }, {
	        test: /(\.sass)$/,
	        use: [{
	            loader: "style-loader" // creates style nodes from JS strings
	        }, {
	            loader: "css-loader" // translates CSS into CommonJS
	        }, {
	            loader: "sass-loader" // compiles Sass to CSS
	        }],
      }, {
				test: /\.(jpe?g|png|gif|svg)$/i,
				loader: "file-loader?name=[path][name].[ext]",
				exclude: /node_modules/,
			}, {
	      test: /\.(eot|svg|ttf|woff|woff2)$/,
	      loader: 'file-loader?name=[path][name].[ext]',
				exclude: /node_modules/,
      }
		]
	},
	plugins: [
		new webpack.NoEmitOnErrorsPlugin(),
		new webpack.ProvidePlugin({
			React: 'react',
			ReactDOM: 'react-dom'
		}),
		new CopyWebpackPlugin([
			{from: './index.html', to:'index.html'}
		]),
		new webpack.DefinePlugin({
		  'process.env': {
		    NODE_ENV: JSON.stringify(NODE_ENV)
		  }
		}),
		NODE_ENV === 'production' ? new webpack.optimize.UglifyJsPlugin() : () => {},
		extractSass
	],
};

module.exports = config;
