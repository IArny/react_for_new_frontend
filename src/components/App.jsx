import React from 'react'
import ReactDOM from 'react-dom'
import "./App.sass"
import "normalize.css"
import Home from "./Home/Home"
import Login from "./Login/Login"
import Navigation from "./Navigation/Navigation"
import { BrowserRouter as Router, Route } from 'react-router-dom'

export default class App extends React.Component {
  render() {
		return (
			<Router>
				<div className="app">
					<Navigation />
					<Route exact path="/" component={Home} />
				</div>
			</Router>
		)
	}
}
