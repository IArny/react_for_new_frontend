import React from 'react'
import ReactDOM from 'react-dom'
import ProductList from '../ProductList/ProductList.jsx'

export default class Home extends React.Component {
  render() {
		return (
			<div className="page">
				<ProductList url="http://localhost:8080/ProductList.json" />
			</div>
		)
	}
}
