import {Component} from 'react'
import { connect } from 'react-redux'
import { UPDATE_PRODUCT_LIST } from '../../actions'
import './ProductList.sass'
import ProductListItem from '../ProductListItem/ProductListItem.jsx'

class ProductList extends Component {
	listItems(items) {
		return items.map((item, index) => <ProductListItem key={index} item={item} />)
	}
  render() {

		if (this.props.list.length == 0) {
			this.props.update();
			return <div></div>
		} else {
			return (
				<div className="product-list">
					{this.listItems(this.props.list.response.items)}
				</div>
			)
		}
	}
}

const mapStateToProps = (state, ownProps) => {
	return { list: state.ProductList }
}

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		/**
		 * Обновление данных с сервера
		 * @return {[type]} [description]
		 */
    update: () => {
			let XHR = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
			let xhr = new XHR();
			xhr.withCredentials = true;
			xhr.open('GET', ownProps.url, true);

			xhr.onload = function() {
				if (xhr.readyState === 4) {
					if (xhr.status === 200) {
					  let list = JSON.parse(xhr.response);
						dispatch(UPDATE_PRODUCT_LIST(list)); // обновление состояния списка
					} else {
					  console.error(xhr.statusText)
	        }
				}
			};

			xhr.onerror = function() {
			  console.log('Ошибка ', this.status );
			}
			xhr.send();

		}
	}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductList);
