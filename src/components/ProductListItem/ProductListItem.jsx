import {Component} from 'react'
import { connect } from 'react-redux'
import './ProductListItem.sass'

/**
 * Карточка товара
 * 
 */
export default class ProductListItem extends Component {

  render() {
		let product = this.props.item.product;
		console.log(product);
		return (
			<div className="product-list-item">
				<img className="product-list-item__image" src={product.image.url} />
				<span className="product-list-item__city-name">{product.city.name}</span>
				<a className="product-list-item__name" href={product.api_url}>{product.name}</a>
				<span className="product-list-item__price">{product.price} RUB</span>
				<span className="product-list-item__action-type">{product.action_type}</span>
			</div>
		)
	}
}
