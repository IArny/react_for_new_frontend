(function() {
	'use strict';
	load_file('main.js', 'script')

	function load_file(src, type) {
		if (document.readyState === 'complete') {
			let script = document.createElement('script');
			script.src = src;
			document.body.appendChild(script);
		} else {
			window.addEventListener('load', function () {
				 load_file(src, type);
			});
		}
	}
}());
