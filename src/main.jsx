import { Provider } from 'react-redux'
import { createStore } from 'redux'
import reducer from './reducers'
import App from "./components/App"

let store = createStore(
	reducer,
	process.env.NODE_ENV.development && window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root')
);
