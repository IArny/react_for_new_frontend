import { combineReducers } from 'redux'
import ProductList from './ProductList'

const reducer = combineReducers({ProductList});

export default reducer;
