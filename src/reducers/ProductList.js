const init_state = [];

export default function ProductList(state = init_state, action) {
	switch (action.type) {
		case 'UPDATE_PRODUCT_LIST':
			return action.list
		default:
		return state;

	}
}
